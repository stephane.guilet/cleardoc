Welcome to CleaRDoc, the user documentation for DigiCleaR
===========================================================

This Gitlab repository hosts the user documentation for the DigiCleaR software

Read the user documentation here:

https://cleardoc.readthedocs.io/en/latest

Looking for the *admin* documentation, to install DigiCleaR? This one is located here: https://digiclear4.readthedocs.io/en/latest/ 
