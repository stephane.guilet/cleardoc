.. _index:

.. Digiclear Documentation
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

************************
Digiclear User Manual
************************

What is Digiclear?
-------------------

Digiclear is a laboratory information management system (often shortened as LIMS [1]_). Its main purpose is to allow anyone working in the cleanroom to track what happens, virtually replacing a lab book. 

Why use it?
-------------

Digiclear provides two main complementary functionalities:
	* Logging all the process steps performed on a given sample
	* Logging all the process steps performed on a given machine
	
This means that, as a cleanroom *user*, it allows keeping a precise history of all your samples with the relevant process parameters, even when the process flows involves several people. As it simultaneously allows logging the process steps performed on each machine, it provides *at the same time* a working history of the machines. This serves an important twofold purpose: it allows machine *managers* to monitor the correct working operation of their machines, tracing back potential failures and changes, And it allows users to monitor deviations in their process parameters. Since this is a shared system, privacy parameters allow to keep some information confidential, when needed.

So remember to always fill in Digiclear when you perform a process step!


Table of contents
------------------

.. toctree::
	:maxdepth: 3
	:caption: Contents:
	:numbered:

	getting_started
	machines
	samples
	using_clearcloud
	advanced_functions
	how_to_contribute
	about


.. [1] See e.g. `<https://en.wikipedia.org/wiki/Laboratory_information_management_system>`_ . 
