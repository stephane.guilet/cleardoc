.. _samples:


Samples
========

Samples Homepage
----------------

The second tab in the top menu bar is the **Samples** tab. The layout and functionalities are extremely similar to the machine ones. Clicking on it brings you to the **Sample Manager** page.

.. thumbnail:: images/samples/Samples_00.png
	:alt: Sample Manager homepage
	
.. |ico-manager-sample| image:: images/samples/Samples_00-1.png
	:alt: Sample manager example
	:height: 4ex
	:class: no-scaled-link
	
.. |ico-user-sample| image:: images/samples/Samples_00-2.png
	:alt: Sample user example
	:height: 4ex
	:class: no-scaled-link
	
.. |ico-observer-sample| image:: images/samples/Samples_00-3.png
	:alt: Sample observer example
	:height: 4ex
	:class: no-scaled-link

This page has the same organization as the :doc:`Machines <machines>` page. The central items are a search bar and a list of the last samples that were used. On the left, the menu bar shows all your samples, again with the ability to filter by name. We will discuss how to organize them by groups in another, more advanced part of this documentation. As for machines, a small color bar next the sample name shows your :ref:`role <roles-intro>`:

	* An orange bar indicates that you are *manager* of the sample |ico-manager-sample|
	* A blue bar indicates that you are *user* of the sample |ico-user-sample|
	* A gray bar indicates that you are *observer* of the sample |ico-observer-sample|
	

Sample History
---------------

Clicking on one of your sample brings you to the sample operation history:

.. thumbnail:: images/samples/Samples_01.png
	:alt: Sample history example
	
The first element is again the name of the sample. Below you can see a short description, and two tabs: **Operation history** and **Settings** (that is covered just below). 
The operation history shows, in anti-chronological order, all the operations performed on the sample. Like in the :ref:`machine history <machine-history>`, clicking on a process step expands the details:

.. thumbnail:: images/samples/Samples_02.png
	:alt: Sample process detail

with all the relevant parameters from the process step on the left column, and the sample details on the right one. Remember that you **cannot** edit a process step from the **Samples** page, you have to go to the **Machines** page to do so. 



Sample Settings
---------------

Clicking on the **Settings** tab opens the settings page for the sample. It is divided in three blocks, one on the left and two on the right. 

Sample description 
...................


.. thumbnail:: images/samples/Samples_03.png
	:alt: Sample settings page

Let us look at the left part of the page first. This allows you to change the sample name, and add a brief description. The embedded html editor helper allows you to add some formatting (font styles, hyperlinks etc.). Let us put aside the fields regarding "Parent sample" for now. 

You can further see a tickbox "This sample is confidential":

.. image:: images/samples/Samples_03-1.png
	:alt: Sample confidential 
	
which allows to hide the sample name to other people (e.g. in the machine history, for example), as well as a switch "This sample is active":

.. image:: images/samples/Samples_03-2.png
	:alt: Sample active 

.. |ico-inactive-switch| image:: images/samples/Samples_03-3.png
	:alt: Inactive sample switch
	:height: 4ex
	:class: no-scaled-link
	
This switch allows you to "archive" samples that are not used anymore. Like process steps, and every other thing, samples **cannot be deleted** from Digiclear. This ensures that data is not lost over the years. However, to hide them from your everyday interface, you can make them "inactive". If so, they are hidden from the left menu bar, unless the **inactives** button |ico-inactive-switch| is triggered.  

.. |ico-no-change| image:: images/samples/Samples_03-4.png
	:alt: No change button
	:height: 4ex
	:class: no-scaled-link
	
.. |ico-save-changes| image:: images/samples/Samples_03-5.png
	:alt: Save changes button
	:height: 4ex
	:class: no-scaled-link
	
When changing the settings, e.g. sample name or description, you can see that the bottom button change from **no change** |ico-no-change| to **save changes** |ico-save-changes|. Do not forget to click on it before leaving the page. Upon validation, the save fields will turn green:

.. thumbnail:: images/samples/Samples_03-6.png
	:alt: Sample settings have been changed

Sample user list
................

On the top of the right side, you will find the list of people working with this sample. 

.. thumbnail:: images/samples/Samples_04.png
	:alt: Sample user list

So far, it is just me, and since I created the sample, I am *de facto* **Manager**. Clicking on the drop-down menu below opens a list:

.. thumbnail:: images/samples/Samples_04-1.png
	:alt: Sample user list drop-down menu

On top of the list, in blue, you will find the *user groups* that we will discuss in another tutorial. Below, you will find the list of all registered cleanroom users. You can start to type-in the name of one of your colleagues, and add them to the list. By default, they will be added as *users*, but you can now change their role:

.. thumbnail:: images/samples/Samples_04-2.png
	:alt: Assigning a role to a collaborator

.. |ico-delete-item| image:: images/samples/Samples_04-3.png
	:alt: Assigning a role to a collaborator
	:class: no-scaled-link
	
or remove them from the list using the delete button |ico-delete-item|. Again, do not forget to save the changes. 

Sample inheritance
...................

Finally, the last menu of the **Settings** tab on the bottom right is the list of **Parent Samples**: 

.. thumbnail:: images/samples/Samples_05.png
	:alt: Parent sample list
	
**Parent samples** introduce the concept of *inheritance*. Let us assume that you started to process a large wafer (call it *test_parent*), e.g. with a metal deposition and an annealing step. 

.. thumbnail:: images/samples/Samples_06.png
	:alt: Test parent sample

Later on, you cleave it in four to test different processes. The first piece is called *test_child* and you will add another material on it. When creating this sample, you can declare *test_parent* as a parent sample: 

.. thumbnail:: images/samples/Samples_06-1.png
	:alt: Child sample settings

The sample history of *test_child* will already contain all the process steps performed on *wafer* (they "inherit" these process steps from their "parent"). Adding the final deposition:

.. thumbnail:: images/samples/Samples_06-2.png
	:alt: Child sample history
	
where you can see from the **Sample** column that the first two processes were logged on the parent sample. 

Of course, the inheritance is recursive, so you can then cut the child sample into other child samples. You can also indicate two (or more) parent samples [1]_ for example in the case where the sample is made by bonding two parent samples together. 

.. warning::
	Be careful to save the changes with the corresponding buttons on the three parts of the settings tab. The leftmost sample settings, the upper list of users, managers and observers, and the list of parent samples have different *save changes* buttons.

.. [1] Samples do not care how many parents or what gender they have, as long as they love their child!