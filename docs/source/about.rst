.. _about:

About
======

DigiCleaR was started by Stephane Guilet in 2013 with a first version using dokuwiki as a form provider and a SQLite database through plugin that manage all. This version was upgraded until 3.0, with the help of Laurent Couraud and Léandre Elmestour, by programming in php new plugins and changing the code of dokuwiki or using code injection. This was not safe at all and in 2018 we decided to recode everything from scratch. Léandre Elmestour was the main programmer, Laurent Couraud was the chief operator for the code management and Stéphane Guilet was the Project Leader. In September 2019, we achieved the first version of DigiCleaR 4.0 
Now released outside its original development lab, it needs a proper documentation!

The following authors have contributed to the DigiCleaR user documentation:

.. include:: ../../AUTHORS.txt
    :literal: