.. _how_to_contribute:

################################
How to contribute to CleaRDoc?
################################

How to contribute?
==================

Contributing is easy: 

	* Get in touch with one of the main maintainers of the CleaRDoc repository
	* Fork the CleaRDoc repository. Clone this fork on your computer and start writing ! 
	* Once you have a new piece of documentation, push it to your fork. You can then create a pull request to have it merged to CleaRDoc

How to edit? On your computer
=============================

.. _before_starting:

Before you start
----------------

Before being able to contribute, you need to have a working `Python distribution <https://www.python.org/>`_ installed. You also need *git* [1]_. You can get git for Windows `here <https://gitforwindows.org/>`_. To install it on Linux, simply use your package manager (although you probably already have it). 

Forking, cloning and whatnot
----------------------------

Start by making a *fork* of the CleaRDoc repository. It is your own personal version of the documentation, on which you can work freely without interfering with the official one. This is achieved by clicking on the *Fork* button in the upper right of the `CleaRDoc repository <https://gitlab.com/stephane.guilet/cleardoc>`_. This only sets up the copy on the gitlab server though. To get your own *local* copy that you can edit, you need to *clone* this fork on your computer. Assuming you have :ref:`git installed <before_starting>`, you can create a new directory dedicated to this. Let's call it ``MyCleaRDoc``. Open a command prompt and go to that directory::

	cd Path_to_MyCleaRDoc
	
You can get the clone URL from gitlab, depending on your preferred method:

Clone with SSH (requires to :ref:`setup a SSH key authentication with gitlab <ssh_basics>`)::
	
	git clone git@gitlab.com:YourGitlabUserName/cleardoc.git

Clone with HTTPS::

	git clone https://gitlab.com/YourGitlabUserName/cleardoc.git

You should now have the following directory structure::

	MyClearDoc
	|
	|--cleardoc
	
	

Preparation of the environment
------------------------------

It is best practice to separate your python environments depending on their purpose, so you do not end up mixing packages of different versions that would be incompatible. Let us setup a dedicated python environment simply for creating the documentation.
Assuming you already forked the CleaRDoc repository, and cloned it locally as detailed above, you should have::

	MyClearDoc
	|
	|--cleardoc
	
Open a command prompt, and go to the location of the ``MyCleaRDoc`` directory::
	
	cd the_parent_path/digidoc-env-main

Create the dedicated python environment (here callend ``env``)::

	python -m venv env

Your new directory structure now looks like:: 

	MyClearDoc
	|
	|--cleardoc
	|
	|--env

We can now activate this environment and work on it:

.. tabs::

	.. code-tab:: batch Windows
		
		call env\Scripts\activate
		
	.. code-tab:: bash Linux
		
		source env/bin/activate
	
The first thing to do is to install the required packages to compile the documentation locally::
	
	python -m pip install --upgrade pip
	pip install wheel
	pip install -r cleardoc/docs/requirements.txt

Everything should run fine at this point, and you have completed the setup. You can now compile the documentation and start editing your local copy!


Day to day editing
------------------

Open a command prompt in the ``MyCleaRDoc`` directory and activate the environment (if not already activated):

.. tabs::

	.. code-tab:: batch Windows
		
		call env\Scripts\activate
		
	.. code-tab:: bash Linux
		
		source env/bin/activate

We will use an on-the-fly compiler that allows you to instantaneously see the changes on the documentation in your web browser:

.. tabs::

	.. code-tab:: batch Windows
		
		sphinx-autobuild --re-ignore ".*(~|#)$" cleardoc/docs/source cleardoc/_build/html
		
	.. code-tab:: bash Linux
		
		exec sphinx-autobuild --re-ignore ".*(~|#)$" cleardoc/docs/source cleardoc/_build/html
		



	
Open the page `127.0.0.1:8000 <http://127.0.0.1:8000>`_. This page reflects instantaneously the changes you make in the source files. 
You can now edit the source files under using your preferred editor and see the results. Please, configure your editor to respect the only ``\n`` character at the end of lines!

Do not forget to upload your changes regularly, by committing, and push the results on your gitlab repository::

   git status # understand the results
   git add the_new_files # a simple "git add ." is possible
   git commit -am "Change this file and this file" # adjust the comment
   git push

When your piece of documentation is ready, you can ask for it to be added to CleaRDoc by creating a pull request. 

Keeping your doc up-to-date:
-----------------------------

Synchronize with other editing already pushed on the gitlab server::

   git pull


.. _ssh_basics:

Creating a SSH key and pairing it with gitlab [2]_
----------------------------------------------------

Start by checking that you do not already have an ssh key pair. To do so, look in your home folder for a hidden directory ``.ssh``, typically in:

.. tabs:: 
	
	.. code-tab:: batch Windows
	
		C:\Users\yourusername\.ssh

	.. code-tab:: bash Linux
	
		/home/yourusername/.ssh
		
Open a terminal and run::

	ssh-keygen -C "youremail@domain.ext"

This will generate an output looking like:

.. tabs::

	.. code-tab:: batch Windows
	
		Generating public/private rsa key pair.
		Enter file in which to save the key (C:\Users\yourusername/.ssh/id_rsa):

	.. code-tab:: bash Linux
	
		Generating public/private rsa key pair.
		Enter file in which to save the key (/home/username/.ssh/id_rsa):

By default the filename is ``id_rsa`` for the private key, and ``id_rsa.pub`` for the public key. Unless you have a good reason you can keep the default setting. Otherwise change the name of the file here. 

.. Note::

	If you do this without checking first that you do not already have a ssh key, you risk overwriting the old ssh key, which may be annoying.
	
You will then be asked for a passphrase. This is optional, it is used to encrypt your private key. Again, you can leave the default setting for now. If you use a passphrase, you will be required to type it every time you use the ssh key. After passphrase confirmation, a random image is generated (but you don't care about that for now). 
Navigate to 

.. tabs:: 
	
	.. code-tab:: batch Windows
	
		C:\Users\yourusername\.ssh

	.. code-tab:: bash Linux
	
		/home/yourusername/.ssh
		
And you will find two files, ``id_rsa`` and ``id_rsa.pub``. The first one is your private key, and as its name suggests, **you should keep it private**, that is safely stored (and backed up). The second file is your public key, that you can communicate to the server on which you want to authenticate. In our case, go to https://gitlab.com/-/profile/keys and click on *Add new key*. Open the ``id_rsa.pub`` file (with any text editor) and copy its content. Paste it into the **Key** field in the webpage. You can add a title to this key, and various settings, but the defaults will do. And *voilà*. 

To test that everything went well, you can try to connect to the gitlab server using::

	ssh -T git@gitlab.com

If this is the first time you connect to gitlab with this method, you will see a message like::

	The authenticity of host 'gitlab.com' can't be established.
	ECDSA key fingerprint is SHA256:alotofgibberishcharacterswithcase.
	Are you sure you want to continue connecting (yes/no)? 
	
To which you should say ``yes``. If you see a welcome message, everything went fine.



.. [1] If you made it this far, don't be scared, try it `<https://git-scm.com/>`_!
.. [2] Tell me more! `<https://docs.gitlab.com/ee/user/ssh.html>`_
